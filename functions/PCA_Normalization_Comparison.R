PCA_Normalization_Comparison<-function(Zscore, Quartile, Whitening, LOG2N, MINMAX, TH=NULL){
  #Zscore, Quartile, Whitening, LOG2N, MINMAX are normalized dataframe with numeric variables
  #TH=Cumulative variance, defaut=0,8
  
  #Libraries
  library(factoextra)
  library(FactoMineR)
  library(ggpubr)
  library(dplyr)
  library(devtools)


    
    #Chech if TH is NULL, default value is 1
    if(is.null(TH)) {
      b=0.8 #1
    } else {
      b=TH
    }
    
    #PCA

    #Because you can't divide by the standard deviation if it's infinity. 
    #to remove zero variance columns from the dataset
    Zscore=Zscore[ , apply(Zscore, 2, var) != 0]
    Quartile=Quartile[ , apply(Quartile, 2, var) != 0]
    Whitening=Whitening[ , apply(Whitening, 2, var) != 0]
    LOG2N=LOG2N[ , apply(LOG2N, 2, var) != 0]
    MINMAX=MINMAX[ , apply(MINMAX, 2, var) != 0]
    
    #Running PCA
    res.pca_Zscore <- prcomp(Zscore) 
    res.pca_Quartile <- prcomp(Quartile)
    res.pca_Whitening <- prcomp(Whitening)
    res.pca_LOG2N <- prcomp(LOG2N)
    res.pca_MINMAX<- prcomp(MINMAX)
    
    #Cumulative Proportion: This is simply the accumulated amount of explained variance, ie. if we used the first 10 components we would be able to account for >95% of total variance in the data.
    cumpro_Zscore <- cumsum(res.pca_Zscore$sdev^2 / sum(res.pca_Zscore$sdev^2))
    cumpro_Quartile <- cumsum(res.pca_Quartile$sdev^2 / sum(res.pca_Quartile$sdev^2))
    cumpro_Whitening<- cumsum(res.pca_Whitening$sdev^2 / sum(res.pca_Whitening$sdev^2))
    cumpro_LOG2N <- cumsum(res.pca_LOG2N$sdev^2 / sum(res.pca_LOG2N$sdev^2))
    cumpro_MINMAX <- cumsum(res.pca_MINMAX$sdev^2 / sum(res.pca_MINMAX$sdev^2))
    
    # Find the number of components that explain the variance chosen by the user (TH)
    NComponents_Zscore=as.numeric((which(cumpro_Zscore>TH)[1]))
    NComponents_Quartile=as.numeric((which(cumpro_Quartile>TH)[1]))
    NComponents_Whitening=as.numeric((which(cumpro_Whitening>TH)[1]))
    NComponents_LOG2N=as.numeric((which(cumpro_LOG2N>TH)[1]))
    NComponents_MINMAX=as.numeric((which(cumpro_MINMAX>=TH)[1]))
    
    
    #NamesTable=c("Zscore", "Quartile", "Whitening", "LOG2", "MIN-MAX")
    Values=cbind(NComponents_Zscore=NComponents_Zscore, NComponents_Quartile=NComponents_Quartile, 
                 NComponents_Whitening=NComponents_Whitening,NComponents_LOG2N=NComponents_LOG2N, NComponents_MINMAX=NComponents_MINMAX )
    #colnames(Values) <- NamesTable
    Values=as.data.frame(Values)
    
    return(Values)
}

