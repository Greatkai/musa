#Tabletest is a dataframe with numerical features, no NaN/Inf
#rows: patients
#Columns: features/counts

LOGN <-function(Tabletest){
  
  Column=ncol(Tabletest)
  namescol=names(Tabletest)
  
  #removing genes/features that are zero in all patients
  x <- base::colSums(Tabletest==0)!=nrow(Tabletest)
  newCountTable <- Tabletest[,x]
  
  #LOG2 Normalization---------------------------------------------------------------
  normalizelog <- function(x) {
    return (log2(x+ 1 - min(x)))
  }
  
  Tabletest_LOG2=as.data.frame(lapply(newCountTable, normalizelog))
  
  Tabletest_LOG2=as.data.frame(Tabletest_LOG2)
  
  names(Tabletest_LOG2)=namescol[x]
  ColumnLOG2=ncol(Tabletest_LOG2)# Calculate new data size, after removing genes/features that are zero in all patients
  
  # Generate BoxPlots for original and normalized features
  # for (i in 1:ColumnLOG2){
  # 
  #   tiff(file = sprintf("Feature %s LOG2.tiff", i), width = 3200, height = 3200, units = "px", res = 300)
  # 
  #   par(mfrow=c(1,2))    # set the plotting area into a 1*2 array
  #   boxplot(newCountTable[, i], xlab=colnames(newCountTable[i]), ylab="Original Values")
  #   boxplot(Tabletest_LOG2[,i],xlab=colnames(Tabletest_LOG2[i]), ylab="Normalized Values")
  # 
  #   dev.off()
  # }
  
  return(Tabletest_LOG2)
  
}
