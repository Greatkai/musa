FROM rocker/shiny

MAINTAINER Mario Zanfardino, PhD "mario.zanfardino@synlab.it"

RUN apt-get update && apt-get install -y git 

RUN apt-get update

RUN apt-get -y install libbz2-dev

RUN R -e "install.packages('shiny')"
RUN R -e "install.packages('DT')"
RUN R -e "install.packages('shinyjs')"
RUN R -e "install.packages('dplyr')"
RUN R -e "install.packages('readr')"
RUN R -e "install.packages('plotly')"
RUN R -e "install.packages('crosstalk')"
RUN R -e "install.packages('shinycssloaders')"
RUN R -e "install.packages('shinydashboard')"
RUN R -e "install.packages('heatmaply')"
RUN R -e "install.packages('shinythemes')"
RUN R -e "install.packages('shinyBS')"
RUN R -e "install.packages('tidyverse')"
RUN R -e "install.packages('data.table')"
RUN R -e "install.packages('corrplot')"
RUN R -e "install.packages('ggplot2')"
RUN R -e "install.packages('shinyjqui')"
RUN R -e "install.packages('htmltools')"
RUN R -e "install.packages('UpSetR')"
RUN R -e "install.packages('glue')"
RUN R -e "install.packages('stringi')"
RUN R -e "install.packages('rlist')"
RUN R -e "install.packages('gridExtra')"
RUN R -e "install.packages('ggsci')"
RUN R -e "install.packages('treemapify')"
RUN R -e "install.packages('cowplot')"
RUN R -e "install.packages('BiocManager')"
RUN R -e "install.packages('XML')"

RUN apt-get -y install liblzma-dev
RUN apt-get install libxml2

RUN R -e "BiocManager::install('Rhtslib')"
RUN R -e "BiocManager::install('Rsamtools')"
RUN R -e "BiocManager::install('rtracklayer')"
RUN R -e "BiocManager::install('GenomicFeatures')"
RUN R -e "BiocManager::install('GenomicDataCommons')"
RUN R -e "BiocManager::install('MultiAssayExperiment')"
RUN R -e "BiocManager::install('TCGAutils')"
RUN R -e "BiocManager::install('RTCGAToolbox')"
RUN R -e "BiocManager::install('scone')"
RUN R -e "BiocManager::install('edgeR')"

RUN R -e "install.packages('remotes')"
RUN R -e "install.packages('config')"
RUN R -e "install.packages('svd')"
RUN R -e "install.packages('factoextra')"
RUN R -e "install.packages('FactoMineR')"
RUN R -e "install.packages('ggpubr')"
RUN R -e "install.packages('whitening')"
RUN R -e "install.packages('devtools')"
RUN R -e "install.packages('shinyWidgets')"

RUN R -e "remotes::install_github('rstudio/shinymeta')"
RUN R -e "remotes::install_github('vqv/ggbiplot')"

RUN mkdir -p /srv/shiny-server/MuSA/
COPY * /srv/shiny-server/MuSA/

COPY /tabs /srv/shiny-server/MuSA/tabs/
COPY functions/ /srv/shiny-server/MuSA/functions/
COPY www/ /srv/shiny-server/MuSA/www/

RUN chmod -R +r /srv/shiny-server/MuSA/

EXPOSE 80

WORKDIR "/srv/shiny-server/MuSA/"

CMD ["R", "-e", "shiny::runApp('/srv/shiny-server/MuSA/', host = '0.0.0.0', port = 80)"]



