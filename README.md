<img src="www/MuSA_logo.png"  width="405" height="272">


## Installation

Download the package with the git clone command (or download the tar.gz file):

```bash
git clone git@gitlab.com:Zanfardino/musa.git
```

Change config.yml with your experiment names, initiate R, navigate to /path/to/app/ and run:

```{r}
install.packages("devtools")

devtools::install()

runApp()
```
## Install with Docker

For a rapid and platform independent installation run:

```bash
git clone git@gitlab.com:Zanfardino/musa.git
```

Change config.yml with your experiment names and run:

```bash
docker build -t musa .

docker run -d --name musa -p 3838:3838 musa

docker logs musa --follow
```
Go to http://0.0.0.0:3838

## Repository structure
* ```README.md``` this file
* ```functions``` - folder with code for functions
* ```test_data``` - folder with examples of radiogenomic dataset in MAE, summarizedExperiment and csv formats
* ```tabs```- folder with code for tabs UI
* ```www``` - folder containing html for the home page 
* ```config.yml```- cofiguration file to set genomic and radiomic experiments
* ```dockerfile```- is a text document that contains all the commands a user could call on the command line to assemble an image. Using docker build users can create an automated build that executes several command-line instructions in succession.



## Input

* an R object (Rdata) of a MultiAssayExperiment (MAE) class
* or/ csv/summarizedExperiment to build a MAE

## test_data Input

* ```radiogenomic_datase.Rdata``` A MAE object to test the app with Upload MAE function
* ```BRCA_RNASeqGene.rds ``` A summarizedExperiment object to test the app with Create MAE function
* ```BRCA_mRNASeqGene.rds ``` A summarizedExperiment object to test the app with Create MAE function
* ```BRCA_T1_weighted_DCE_MRI.csv ``` A radiomic features csv file to test the app with Create MAE function
* ```clinical.csv ``` A csv file with clinical data for patients in the above experiments (mandatory to create MAE)

## The Workflow

### 0- Create MAE 

![upload_data](www/0.gif)

<br />
<br />

### 1- Data Upload And Filtering (only if MAE was not created in the step 0)

* Now you can filter the features using a file with a list of feature
* Now you can fill NA values with "Fill NA" option.
![upload_data](www/1.gif)

<br />
<br />

### 2- Data Normalization And Data Summary

![upload_data](www/2.gif)

<br />
<br />

### 3- Downstream Analysis Example

![upload_data](www/3.gif)
